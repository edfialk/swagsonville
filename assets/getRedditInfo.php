<?php

header("Content-type: application/json");

isset($_GET["id"]) ? $id = $_GET["id"] : exit('post id required');

$text = file_get_contents("http://www.reddit.com/comments/".$id.".json");

$text = json_decode($text, true);

//[0] = info about link, [1] = comments
$text = $text[1];

$comment_limit = 3;

$result = array();

if (count($text['data']['children']) < $comment_limit)
  $comment_limit = count($text['data']['children']);


for ($i = 0; $i < $comment_limit; $i++){
  $data = $text['data']['children'][$i]['data'];
  $comment = $data['body'];
  $author = $data['author'];
  $id = $data['id'];
  $result[$i]['comment'] = $comment;
  $result[$i]['author'] = $author;
  $result[$i]['id'] = $id;  
}


echo json_encode($result);


?>