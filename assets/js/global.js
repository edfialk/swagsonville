
if (typeof console == 'undefined') {
    this.console = {log: function(){}};
}

var current_theme = "imageTiles";
var feed;
var postCount = 0;
var requestLimit = 27;
var requesting = false;
var noMoreData = false;

var hash = window.location.hash;
setInterval(function(){
    if (window.location.hash != hash) {
        hash = window.location.hash;
        if (hash == ""){
            Shadowbox.close();
        }
    }
}, 100);
    
Shadowbox.init({
  overlayOpacity: 0.7,
  onClose: function(){
    $("body").css("overflow", "auto");
    //window.location.hash = "";
  },
  onFinish: function(){
    $(".openPost").waitForImages(function() {
        var w = $(".openPost").outerWidth();
        var h = $(".openPost").outerHeight();
        if (h > $("#sb-body").outerHeight()) h = $("#sb-body").outerHeight();
        if (w > $("#sb-body").outerWidth()) w = $("#sb-body").outerWidth();
    
        Shadowbox.skin.dynamicResize(w+10, h+20);
    });
  } 
});

$(document).ajaxError(function(event, request, settings){
  alert(request.responseText);
});
    
$(document).ready(function(){
    window.location.hash = "";
    current_theme = getUrlVars()["theme"] || current_theme;
    setTheme(current_theme);

    var getCache = getUrlVars()["cache"] || "1";
    requesting = true;  
    $.getJSON("fetch.php?cache="+getCache+"&start=0&limit="+requestLimit, function(data){
        console.log(data);
        if (data.length > 0){
            feed = data;
            addData(data);
        }else{
            noMoreData  = true;
        }
        $(".loading").hide();
    });

    $('#swapCSSli').click(function(){
        swapCss();
    });
	
	$(window).scroll(function(){
        if (requesting || noMoreData) return;
	 
        var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var scrolltrigger = 0.65;
    
        if ((wintop/(docheight-winheight)) > scrolltrigger){
            requesting = true;
            $.getJSON("fetch.php?start="+postCount+"&limit="+requestLimit, function(data){
                console.log(data);
                //algorithm(); ?
                if (data.length > 0){
                    addData(data);
                } else {
                    noMoreData = true;
                }
            });
        }
	});
	
});

function addPost(post){
  //really need theme object
  if (current_theme == "yabadoo"){
    addYabadooPost(post);
  }else if (current_theme == "imageTiles"){
    addImageTilesPost(post);
  }
}
function addData(data){
  if (current_theme == "yabadoo"){
    $(data).each(function(i){
      if (this == null) return;
      if (!haveMain){
        if (this.image != null){
          //addMain(this);
          addYabadooPost(this, true);
          haveMain = true;
        }else{
          addYabadooPost(this);
        }
      }else{
        addYabadooPost(this);
      }
      postCount++;
    });
    setupYabadoo();    
  }else if(current_theme == "imageTiles"){
    $(data).each(function(){
      addPost(this);
    });
    setupImageTiles();
  }
  fixBrokenImages();
  requesting = false;
}
function swapCss(){
  postCount = 0;
  noMoreData = false;
	var css;
	if (current_theme == "imageTiles"){
		current_theme = "yabadoo";
	}else{
		current_theme = "imageTiles";
	}
	setTheme(current_theme);
}
function setTheme(theme){
  resetTheme();
  var css;
  css = 'assets/css/'+theme+'.css';
  $('#currentTheme').attr('href',css);
  if (theme == "yabadoo"){
    $("#mainContainer").append("<div class='row' id='mainRow1'></div>");
    $("#mainRow1").append("<div class='span9' id='mainCol1'></div><div class='span3' id='col4'></div>");
    $("#mainCol1").append("<div class='span9 postMainCol' id='postMainCol' style='margin-left: 0'></div>");
    $("#mainCol1").append("<div class='span3' id='col1'></div>");
    $("#mainCol1").append("<div class='span3' id='col2'></div>");
    $("#mainCol1").append("<div class='span3' id='col3'></div>");
  }else if(theme == "imageTiles"){
    $("#mainContainer").append("<div class='row' id='mainRow1'></div>");
    $("#mainRow1").append("<div class='span3' id='col1'></div><div class='span3' id='col2'></div><div class='span3' id='col3'></div>");
  }
  addData(feed);
}
function htmlEncode(value){
  return $('<div/>').text(value).html();
}
function htmlDecode(value){
  return $('<div/>').html(value).text();
}
function fitStringToWidth(str,width,className) {
  // str    A string where html-entities are allowed but no tags.
  // width  The maximum allowed width in pixels
  // className  A CSS class name with the desired font-name and font-size. (optional)
  // ----
  // _escTag is a helper to escape 'less than' and 'greater than'
  function _escTag(s){ return s.replace("<","&lt;").replace(">","&gt;");}

  var span = document.createElement("span");
  if (className) span.className=className;
  span.style.display='inline';
  span.style.visibility = 'hidden';
  span.style.padding = '0px';
  document.body.appendChild(span);

  var result = _escTag(str); // default to the whole string
  span.innerHTML = result;
  if (span.offsetWidth > width) {
    var posStart = 0, posMid, posEnd = str.length, posLength;
    // Calculate (posEnd - posStart) integer division by 2 and
    // assign it to posLength. Repeat until posLength is zero.
    while (posLength = (posEnd - posStart) >> 1) {
      posMid = posStart + posLength;
      //Get the string from the begining up to posMid;
      span.innerHTML = _escTag(str.substring(0,posMid)) + '&hellip;';

      // Check if the current width is too wide (set new end)
      // or too narrow (set new start)
      if ( span.offsetWidth > width ) posEnd = posMid; else posStart=posMid;
    }

    result = '<abbr title="' +
      str.replace("\"","&quot;") + '">' +
      _escTag(str.substring(0,posStart)) +
      '...<\/abbr>';
  }
  document.body.removeChild(span);
  return result;
}