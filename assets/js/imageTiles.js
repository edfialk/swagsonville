function setupImageTiles() {
    var w, h;    
    $(".post").hover(
        function(){
            if ($(window).width() < 565) return; 
            $(this).find(".postHead").fadeIn('slow');
        },
        function(){
            if ($(window).width() < 565) return;
            $(this).find(".postHead").fadeOut('slow');
        }
    );
    
    $(".post").click(
      function(){
        window.location.hash = "post";
        //w = $("#col1").width() * 3;
        w = $(window).width();
        h = $(window).height();
        var link = $(this).find(".info").find(".link").text();
        var title = $(this).find("p.title").text();
        var img = null;
        if ($(this).find("p.image").length > 0){
          img = $(this).find("p.image").text();          
        }
        var source = $(this).find("p.source").html();
        var text = htmlDecode($(this).find("p.text").html());
        var date = $(this).find("p.date").text();
        var dateSpan = "";
        if (date != "") dateSpan = "<span class='date'>on "+date+"</span>";
        
        //if ($.browser.mobile){
        if (w < 565){
            //$(this).css("position", "absolute").css("top", "0").css("left", "0").width(w).height(h);
            $(this).addClass("mobileClick");
            $(this).find(".postHead").show();
            $(this).width(w-20).css("height", "auto");
            $(this).find("div.info").css("display", "block");  
            $(this).find(".text").css("display","block").html(text);
            $(this).off('click');
            $(this).find(".postImg").click(function(){
                window.open(link);
            });
            $(this).find("header").click(function(){
                window.open(link);
            });
                              
        }else{
            var commentsJSON = $.parseJSON($(this).find(".comments").html());
            var comments = "";
            $(commentsJSON).each(function(){
              var comment = "<div id='openPostComment'><span class='openPostCommentAuthor'>" + this.author + ": </span><span class='openPostCommentText'>" + this.comment + "</span></div>";
              comments += comment;          
            });
            var imgSpan = "";      
            var postH = h - 120;
            var postW = w - 171;
            var contentH = postH - 60;
            
            if ($(this).find("p.youtubeId").length > 0){
              var youtube = $(this).find("p.youtubeId").text();
              if (text.indexOf(youtube) < 0 || text.length == 0 || link.indexOf(youtube) > -1){
                var ytlink = "http://www.youtube.com/embed/"+youtube+"?wmode=transparent";
                var youtubeW = postW - 100;
                if (youtubeW > 800) youTubeW = 800;  
                var youtubeH = Math.round(youtubeW*9)/16; //default youtube ratio, not best for widescreen
                imgSpan = "<div style='text-align: center'><iframe class='youtube-player' src='"+ytlink+"' frameborder='0' width='"+youtubeW+"' height='"+youtubeH+"'></iframe></div>";
              }
            }else if ($(this).find("p.vimeoId").length > 0){
              var vimeo = $(this).find("p.vimeo").text();
              if (text.indexOf(vimeo) < 0 || text.length == 0){
                var vimW = postW - 20;
                var vimH = Math.round((315/420)) * vimW;
                imgSpan = '<iframe src="http://player.vimeo.com/video/'+id+'" width="'+vimW+'" height="'+vimH+'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
              }
            }else if (img != null && img != 'null'){
              if (img != "assets/img/nomore.jpg")
                imgSpan = "<a target='_blank' href='"+link+"'><img src='"+img+"'></a>";          
            }
          
            var ht = "\
                <div class='openPost'>\
                    <div class='openPostContentWrap'>\
                        <div class='openPostContent'>\
                            <div class='openPostImgWrap'>"+imgSpan+"</div>\
                            <div class='openPostTextWrap'>\
                                <div class='openPostHead'>\
                                    <div class='openPostTitle'>\
                                        <h4 class='title'><a href='"+link+"'>"+title+"</a></h4>\
                                    </div>\
                                    <div class='openPostSubTitle'>\
                                        <span class='source'>"+source+"</span>\
                                        "+dateSpan+"\
                                    </div>\
                                </div>\
                                <div class='openPostText'>\
                                    "+text+"<br>\
                                    <div class='openPostComments'>\
                                        " + comments + "\
                                    </div>\
                                </div>\
                            <div>\
                        </div>\
                    </div>\
                </div>";
            if ($(window).width() < 565){
                w = $(window).width();
            }else{
                w = $(window).width() - 100;
            }
    
            Shadowbox.open({
              content: ht,
              player: "html",
              height: h,
              width: w
            });
            $("body").css('overflow', 'hidden');
                    
          }
        }   
    );     
}

function addImageTilesPost(post){

    var link = post.link;
    var title = post.title;
    var text = post.text;
    var source = post.source;
    var sourceURL = post.sourceURL;
    var image = post.image || null;
    var tileImage = post.tile_image || null;

    var divStyle = "";
    var headStyle = ""; 
    if (title.length <= 20){
        divStyle="short";
        headStyle="short"; 
    }else{
        divStyle="long";
        headStyle="long";        
    }

    var abbr = fitStringToWidth(title, 300, headStyle);
    var htout = "<div class='post'>";
    //htout += "<a href='#post'>";  
    htout += "<header class='postHead "+divStyle+"'><h3 class='headline "+headStyle+"'>"+abbr+"</h3><p>"+source+"</p></header>";
    //htout += "<header class='postHead "+divStyle+"'><h3 class='headline "+headStyle+"'><span>"+title+"</span></h3><p>"+source+"</p></header>";

    if (tileImage != null){
        if (tileImage.indexOf("?") != -1){
            tileImage = tileImage.replace("?", "%3f");
        }
        var imgSpan = "";

        if (post.hasOwnProperty('youtube') || post.hasOwnProperty('vimeo')){
            imgSpan += "<div class='video' ></div>";
        }
        imgSpan += "<img src="+tileImage+" class='imgPost' />";
        htout += "<div class='postImg'>"+imgSpan+"</div>";    
    }
 
    htout += "<div class='info'>";  
    if (post.hasOwnProperty('redditID')){
        htout += "<p class='redditId info'>"+post.redditID+"</p>"; 
    }
    if (post.hasOwnProperty('source')){
        htout += "<p class='source info'>";
        if (post.hasOwnProperty('sourceURL')) htout += "<a href='"+post.sourceURL+"'>"+post.source+"</a>"; 
        else htout += "<p class='source info'>"+post.source+"</p>";
    }
    if (post.hasOwnProperty('title')) htout += "<p class='title info'>"+post.title+"</p>";
    if (post.hasOwnProperty('image')) htout += "<p class='image info'>"+post.image+"</p>";
    if (post.hasOwnProperty('comments')) htout += "<p class='comments info'>"+post.comments+"</p>"; 
    if (post.hasOwnProperty('youtube')) htout += "<p class='youtubeId info'>"+post.youtube+"</p>";
    if (post.hasOwnProperty('vimeo')) htout += "<p class='vimeoId info'>"+post.vimeo+"</p>";
    if (post.hasOwnProperty('date')) htout += "<p class='date info'>"+post.date+"</p>";
    if (post.hasOwnProperty('text')) htout += "<p class='text info'>"+htmlEncode(post.text)+"</p>";
  
  htout += "<p class='link info'>"+link+"</p>";  
  htout += "</div>";
    
  postCount++;    
  $("#col" + colIndx).append(htout);
  colIndx++;
  if (colIndx > 3) colIndx = 1;    
  if (postCount % 9 == 0){
    addBar();
  }    
}
function addBar(){
  var rand = Math.floor(Math.random()*8)+1;
  var banner = "<img src='assets/themes/swagsonville/swag-banner-"+rand+".gif' >";
  
  $("#col1").append("<div class='addBar addBarLeft'>"+banner+"</div>");
  $("#col2").append("<div class='addBar addBarCenter'></div>");
  $("#col3").append("<div class='addBar addBarRight'></div>");   
}
