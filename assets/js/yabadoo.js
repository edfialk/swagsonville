var colIndx = 1;
var haveMain = false;
function setupYabadoo(){
    $(".postMain").hover(
      function(){
        $(this).find("div.description").show();
        $(this).find("div.postFoot").show();
        $(this).css("border", "3px solid #82E6FF");
        $(this).css("margin-left", "-3px");
        $(this).css("margin-top", "13px");
      },
      function(){
        $(this).find("div.description").hide();
        $(this).find("div.postFoot").hide();
        $(this).css("border", "");
        $(this).css("margin-left", "0");
        $(this).css("margin-top", "15px");
      }
    );
    $(".post").hover(
      function(){
        $(this).find("div.description").show();
        $(this).find("div.postFoot").show();
        $(this).css("border", "3px solid #82E6FF");
        $(this).css("margin-left", "-3px");
        $(this).css("margin-top", "13px");
      }, 
      function(){
        $(this).find("div.description").hide();
        $(this).find("div.postFoot").hide();
        $(this).css("border", "");
        $(this).css("margin-left", "0");
        $(this).css("margin-top", "15px");
      }
    );
    $(".post").click(
      function(){
        var w = $("#col1").width() * 4;
        var h = $(window).height();
        var link = $(this).find(".link").text();
        var title = $(this).find("h3").text();
        var img = $(this).find("img").attr("src");
        var source = $(this).find(".postFoot").html();
        var text = $(this).find(".description").html();
        
        if (source.indexOf("reddit.com" != -1)) text = text.replace("<br>", "");
        
        var info = null;
        var postH = h - 120;
        var postW = w - 171;
        var contentH = postH - 60;

        var commentsJSON = $.parseJSON($(this).find(".comments").html());
        var comments = "";
        $(commentsJSON).each(function(){
          var comment = "<div id='openPostComment'><span class='openPostCommentAuthor'>" + this.author + ": </span><span class='openPostCommentText'>" + this.comment + "</span></div>";
          comments += comment;          
        });        
        
        // style='height:"+postH+"px;width:"+postW+"px;'
        // style='height:"+contentH+"px;width:"+postW+"px;'
        
        var imgSpan = "";
        if ($(this).find("p.youtubeId").length > 0){
          var youtube = $(this).find("p.youtubeId").text();
          if (text.indexOf(youtube) < 0){
            var ytlink = "http://www.youtube.com/embed/"+youtube+"?wmode=transparent";
            var youtubeW = postW - 20;
            var youtubeH = Math.round((315/420)) * youtubeW; //default youtube ratio, not best for widescreen
            imgSpan = "<iframe class='youtube-player' src='"+ytlink+"' frameborder='0' width='"+youtubeW+"' height='"+youtubeH+"'></iframe>";
          }
        }else if (img != undefined){
          if (img != "assets/img/nomore.jpg")
            imgSpan = "<a target='_blank' href='"+link+"'><img src='"+img+"'></a>";          
        }
        
        var ht = "\
          <div class='openPost'>\
            <div class='openPostHead'>\
              <div class='openPostTitle'>\
                <a href='"+link+"'><h2 class='title'>"+title+"</h2></a>\
              </div>\
              <div class='openPostSubTitle'>\
                "+source+"\
              </div>\
            </div>\
            <div class='openPostContentWrap'>\
              <div class='openPostContent'>\
                <div class='openPostImg'>"+imgSpan+"</div>\
                <div class='openPostText'>\
                  "+text+"<br>\
                  <div class='openPostComments'>\
                    " + comments + "\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        ";

        Shadowbox.open({
          content: ht,
          player: "html",
          height: h,
          width: w
        });
        
        $("body").css('overflow', 'hidden');
 
        setTimeout('Shadowbox.skin.dynamicResize($(".openPost").outerWidth()+5, $(".openPost").outerHeight()+20)', 1000);

      }//end post.click(function(){
    );
}


function resetTheme(){
  $("#mainContainer").html("");  
}
function addMain(post){
  $("#postMain").show();
  var link = post.link;
  var title = post.title;
  var text = post.text;
  var source = post.source;
  var image = post.image || null;

  var imgSpan = ""
  if (post.hasOwnProperty('youtube')){
    var w = $("div.post").width();  
    var ytlink = "http://www.youtube.com/embed/"+post.youtube+"?wmode=transparent";           
    var h = (315/420) * w; //default youtube ratio, not best for widescreen
    imgSpan = "<iframe class='youtube-player' src='"+ytlink+"' frameborder='0' width='"+w+"' height='"+h+"'></iframe>";
  }else if (image != null){
    imgSpan = "<img src="+image+" class='imgPost' />";
  }
  
  var htout = "<div class='postMainHead'><a target='_blank' href='"+link+"'><h3>"+title+"</h3></a></div>" +
      "<div class='postImgMain'>"+imgSpan+"</div>" +
      "<div class='description'>"+text+"</div>" + 
      "<div class='postFoot'>"+source+"</div>";
      //"<p class='stats'>footer</p>" +      
  $("#postMain").append(htout);
  $("#postMain").show();
}
function addYabadooPost(post, main){
  main = typeof main != 'undefined' ? true : false;

  var link = post.link;
  var title = post.title;
  var text = post.text;
  var source = post.source;
  var image = post.image || null;

  var htout = "";
  if (main)
    htout = "<div class='postMain'><div class='postMainHead'><h3>"+title+"</h3></div>";
  else
    htout = "<div class='post'><div class='postHead'><h3>"+title+"</h3></div>";

  var imgSpan = "";
  var imgClass = (main == false ? "imgPost" : "postImgMain");
  if (image != null){
    imgSpan = "<img src='"+image+"' class='imgPost' >";
    htout += "<div class='postImg'>"+imgSpan+"</div>";
  }

  var sourceSpan = source;  
  if (post.hasOwnProperty('sourceURL')){
    sourceSpan = "<a href='"+post.sourceURL+"'>"+source+"</a>";
  }
  htout += "<div class='description'>"+text+"</div><div class='postFoot'>"+sourceSpan+"</div>";
  htout += "<div class='info' style='display: none;'>";
  
  if (source.indexOf("reddit") != -1){
    htout += "<p class='redditId'>"+post.redditID+"</p>"; 
  }
  
  if (post.hasOwnProperty('comments')) htout += "<p class='comments'>"+post.comments+"</p>"; 
  if (post.hasOwnProperty('youtube')) htout += "<p class='youtubeId'>"+post.youtube+"</p>";

  htout += "<p class='link'>"+link+"</p>";  
  htout += "</div>";
    
  //"<p class='stats'>footer</p>" +   
  //pick shortest column
  if (main){
    $("#postMainCol").append(htout);
    $(".postMain").show();
  }else{
    var col; 
    var main = $("#postMain").height();
    var c1 = $("#col1").height() + main;
    var c2 = $("#col2").height() + main;
    var c3 = $("#col3").height() + main;
    var c4 = $("#col4").height();
  
    var c = Math.min(c1, c2, c3, c4);
    if (c == c1) col = "#col1";
    if (c == c2) col = "#col2";
    if (c == c3) col = "#col3";
    if (c == c4) col = "#col4";    
      
    $(col).append(htout);
  }
}

/** random utility **/
function fixBrokenImages(){
/*
  $("img").one('error', function(){
    this.src = "assets/img/nomore.jpg";
    $(this).width("100%");
  });
  */
}
function outerHTML(node){
    // if IE, Chrome take the internal method otherwise build one
  return node.outerHTML || (
      function(n){
          var div = document.createElement('div'), h;
          div.appendChild( n.cloneNode(true) );
          h = div.innerHTML;
          div = null;
          return h;
      })(node);
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
