<?php

set_time_limit("900");
if (!function_exists("debug")){
    function debug($msg){
        error_log($msg);
        echo($msg);
    }
}

function scale_image($image, $new_width, $new_height, &$ratio){
    $height = $image->getImageHeight();
    $width = $image->getImageWidth();
    
    #resize to overfill target area
    $height = $image->getImageHeight();
    $width = $image->getImageWidth();
    $xratio = $new_width/$width;
    $yratio = $new_height/$height;
    if ($xratio >= $yratio){
        $image->scaleImage($new_width*$ratio, 0);
        $ratio = $xratio * $ratio;
    }else{
        $image->scaleImage(0, $new_height*$ratio);
        $ratio = $yratio * $ratio;
    }
    $height = $image->getImageHeight();
    $width = $image->getImageWidth();
    debug("<p>resized: $width x $height</p>\n");
    return $image;
}

function extract_frame_if_animated_gif($image, $frame=1){
    $frames = $image->getNumberImages();  

    if ($frames > 1){
        debug("animated image");
        foreach ($image as $frame){
            return $frame;
        }
    }
    return $image;
}

function process_image($image_name,$new_width,$new_height,$processed_name,$strategy="scale=1.25,trim"){
    try{
        $image = new Imagick($image_name);
    }catch(Exception $e){
        return $image_name;
    }

    $cropping = clone $image;
    
    $height = $image->getImageHeight();
    $width = $image->getImageWidth();
    debug("<p>processing $image_name($width x $height) to $new_width x $new_height</p> at $processed_name\n");
    
    $image = extract_frame_if_animated_gif($image);

    $ratio = 1;
    if (preg_match("/scale/",$strategy)){
        if (preg_match("/scale=([^,]+)/", $strategy, $matches)){
            $ratio = $matches[1];
        }
        $image = scale_image($image, $new_width, $new_height, $ratio);

    }

    $x_coord = 0;
    $y_coord = 0;
    if (preg_match("/trim/", $strategy)){
        $new_image = trim_image($image, $new_width, $new_height, "entropic", $x_coord,$y_coord);
    }

    debug("<p>ratio: $ratio, $x_coord, $y_coord</p>\n");    
    if ($ratio >= 2){
        debug("<p>Image too small, filtering</p>\n"); 
        $new_image->reduceNoiseImage(0);
    }

    mark_crop($cropping, $x_coord, $y_coord, $new_width, $new_height, $ratio);
    debug("writing to $processed_name.cropping\n");    
    $cropping->writeImage("$processed_name.cropping");

/*
    if ($is_video){
        #apply transparent play button
        $playImg = new Imagick('overlay.png');
        $xSet = 0;
        $ySet = 0;
        $new_image = $playImg->compositeImage( $new_image, imagick::COMPOSITE_ADD, 0, 0 );        
    }
*/    

    $new_image->writeImage($processed_name);
    return $processed_name;
}

function mark_crop($image, $x, $y, $w, $h, $r){

    $draw = new ImagickDraw();
    $startx = $x/$r; 
    $starty = $y/$r;
    $endx = ($x + $w)/$r;
    $endy = ($y + $h)/$r;
    $draw->setStrokeColor( new ImagickPixel( 'green' ) );
    $draw->setStrokeWidth(4);
    // Draw the rectangle 
    $draw->line( $startx, $starty, $endx, $starty);    
    $draw->line( $endx, $starty, $endx, $endy);    
    $draw->line( $startx, $starty, $startx, $endy);    
    $draw->line( $startx, $endy, $endx, $endy);    

     #Lets draw another
     #$draw->setFillColor('navy');    // Set up some colors to use for fill and outline
     #$draw->setStrokeColor( new ImagickPixel( 'yellow' ) );
     #$draw->setStrokeWidth(4);
     #$draw->rectangle( 150, 225, 350, 300 );    // Draw the rectangle 
    
     // and another
     #$draw->setFillColor('magenta');    // Set up some colors to use for fill and outline
     #$draw->setStrokeColor( new ImagickPixel( 'cyan' ) );
     #$draw->setStrokeWidth(2);
     #$draw->rectangle( 380, 100, 400, 350 );    // Draw the rectangle 
    
     $image->drawImage( $draw );    // Apply the stuff from the draw class to the image canvas

     return $image;   
}

function trim_image($image, $new_width, $new_height, $strategy="entropic", &$x_coord, &$y_coord){
    if ($strategy == "entropic"){
        $image = trim_low_entropy_edges($image, $new_width, $new_height, $x_coord, $y_coord);
    }else{
        echo "crop image strategy not implemented";
    }
    return $image;
}

function trim_low_entropy_edges($image, $new_width, $new_height, &$x_coord, &$y_coord){
    #determine how big overlap is, if less than 10 
    $height = $image->getimageheight();
    $width = $image->getimagewidth();
   
    #make sure we don't have more than 6 slices
    #TODO maybe want 2 slice sizes, if one dimension is way bigger might throw off trimming in other dimension
    $height_overfill = $height-$new_height;
    $width_overfill = $width-$new_height;
    $overfill = $height_overfill;
    if ($width_overfill < $height_overfill){
        $overfill = $width_overfill;
    }
    $slice_size = 20;
    if ($overfill/5 > 20){
        $slice_size = floor($overfill/5);
    }
    debug("<p>$width x $height to $new_width x $new_height w/ slice $slice_size</p>\n");
     
    #remove low entropy edges in x dimension then y dimension(possible strategy switch: y then x, xyxy..., etc)
    while ($width-$new_width >= $slice_size){
        $image = trim_low_entropy_edge_width($image, $slice_size, $x_coord);
        $width = $image->getImageWidth();
    }
    while ($height-$new_height >= $slice_size){
        $image = trim_low_entropy_edge_height($image, $slice_size, $y_coord);
        $height = $image->getImageHeight();
    }

    debug("<p>$width x $height after slicing</p>\n");
    $offset_x = floor(($width-$new_width)/2);
    $offset_y = floor(($height-$new_height)/2);
    debug("<p> offsets: $offset_x, $offset_y</p>\n");
    if ($offset_x > 0 || $offset_y > 0){
        debug("<p> cropping $new_width, $new_height</p>\n");
        $image->cropImage($new_width, $new_height, $offset_x, $offset_y);
    }
    $image->setImagePage(0,0,0,0);
    return $image;
}
    
function trim_low_entropy_edge_width($image, $slice_size, &$x_coord){
    $width = $image->getImageWidth();
    
    $first = clone $image;
    $first->cropimage($slice_size, 0, 0, 0);

    $last = clone $image;
    $last->cropImage($slice_size, 0, $width-$slice_size, 0);

    if (image_entropy($first) > image_entropy($last)){
        $image->cropImage($width-$slice_size, 0, 0, 0);
    }else{
        $image->cropImage($width-$slice_size, 0, $slice_size, 0);
        $x_coord = $x_coord+$slice_size;
    }
    $image->setImagePage(0,0,0,0);
    return $image;
}

function trim_low_entropy_edge_height($image, $slice_size, &$y_coord){
    $height = $image->getImageHeight();
    
    $first = clone $image;
    $first->cropimage(0, $slice_size, 0, 0);

    $last = clone $image;
    $last->cropImage(0, $slice_size, 0, $height-$slice_size);

    if (image_entropy($first) > image_entropy($last)){
        $image->cropImage(0, $height-$slice_size, 0, 0);
    }else{
        $image->cropImage(0, $height-$slice_size, 0, $slice_size);
        $y_coord = $y_coord+$slice_size;
    }
    $image->setImagePage(0,0,0,0);
    return $image;
}

function image_entropy($image){
    $width = $image->getImageWidth;
    $height = $image->getImageHeight;
    while ($width > 100 or $height > 100){
        $image->scaleImage($width/2,0);
    }
    $pixels = $image->getImageHistogram();
    $counts = array();
    $sum = 0;
    #echo "<p> color:count</p>\n";
    foreach ($pixels as $p){
        $count = $p->getColorCount();
        $sum = $sum+$count;
        if ($count > 0){
            array_push($counts,$count);
        }
    }
    #echo "<p> counts: ".Var_Dump($counts)."</p>\n";
    $probabilities = array();
    foreach ($counts as $count){
        array_push($probabilities,$count/$sum);
    }
    #echo "<p> probabilities: ".Var_Dump($probabilities)."</p>\n";
    $entropy=0;
    foreach ($probabilities as $p){
        if ($p != 0){
            $val = $p * log($p, 2);
            $entropy = $entropy+$val;
        }
    }
    return -$entropy;
}

function display_image($tag, $image, $iteration){
    $name = "$tag.$iteration";
    $image->writeImage($name);
    echo "<p>$tag <img style='border:3px solid red' src='$name' /></p>\n"; 
}

?>


