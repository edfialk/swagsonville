<!--TESTING-->

<html>
<head><title>imagick sample</title><head>
<body>
<?php
require_once('imageWrangler.php');

$images = glob("/Users/adam/Documents/entropy_test/*");
#array_push($images, "http://s2.wp.com/wp-content/mu-plugins/highlander-comments/images/wplogo.png?m=1289230950g");
foreach ($images as $img){
    $imagick = new Imagick($img); 
    #genrate width and height to crop original image to reasonable size#
    $max_width = 400;
    $max_height = 200;
    $width = $imagick->width;
    $height = $imagick->height;
    $ratio = $max_height/$height;
    if ($width > $height){
        $ratio = $max_width/$width;
    }
    $display_width = $width;
    $display_height = $height;
    $caption = $width." x ".$height;
    if ($ratio < 1){
        $display_width = floor($width*$ratio);
        $display_height = floor($height*$ratio);
        $caption .= " at ".floor($ratio * 100)."%";
    }

    $target_width= 300;  
    $target_height= 200;

    $new_name = "/tmp/".basename($img).".$width"."x$height.jpg";
    $result_file = process_image($img, $target_width, $target_height, $new_name);

    $result = new Imagick($result_file);
    
    echo "<div><p>original(".basename($img)."): $caption</p>\n";
    echo "<img border=1 src=".$new_name.".cropping width=$display_width height=$display_height> </div>\n";
    
    echo "<div><p>result: ".$result->getImageWidth()." x ".$result->getImageHeight()."</p>\n";
    echo "<img border=1 src=".$result_file."></div><hr>\n";
}
?>

</body>
</html>
