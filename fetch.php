<?php
date_default_timezone_set("America/New_York");

set_time_limit("1800");
ini_set('memory_limit', '512M');
ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9');

//header("Content-type:  application/json");
header("Content-type: text/plain");

ob_start();//Hook output buffer 
require_once(dirname(__FILE__)."/assets/imageWrangler/imageWrangler.php");
require_once(dirname(__FILE__)."/assets/QueryPath/QueryPath.php");
require_once(dirname(__FILE__)."/assets/SimplePie/simplepie.inc");
ob_end_clean();

$random_image_array;
$random_image_index = 0;
$cachepath = 'cache';
$cachefile = dirname(__FILE__)."/".$cachepath."/".date("dmyHi");
$serialItemsFile = dirname(__FILE__)."/".$cachepath."/serialItems"; #soontobe $cachefile soontobe database
$saveitemdate = "-6 months";
$max = 300;
$feedmax = 14;
$server_path = "http://www.swagsonville.com";

if (count($argv) > 0) parse_str(implode('&', array_slice($argv, 1)), $_GET);

isset($_GET["cache"]) ? $getCache = $_GET["cache"] : $getCache = "1";
isset($_GET["debug"]) ? $_DEBUG = $_GET["debug"] : $_DEBUG = 0;
isset($_GET["start"]) ? $start = $_GET["start"] : $start = 0;
isset($_GET["limit"]) ? $limit = $_GET["limit"] : $limit = 50; 
isset($_GET["theme"]) ? $theme = $_GET["theme"] : $theme = "swagsonville";

$cache=array();
if (file_exists($serialItemsFile)){
    $cache = unserialize(file_get_contents($serialItemsFile));
    if (!$cache){
        debug('bad cache file');
        $cache = array();
    }
}

if($getCache){
  if ($cache !== null){
    $cache = theMotherFuckinAlgorithm($cache);
    echo json_encode(array_slice($cache, $start, $limit));
    exit();
  }
  #else bad/no cache file 
}

debug("----------Beginning cache run------------");

$urls = array();
$feeds = dirname(__FILE__)."/assets/feeds.txt";
if (file_exists($feeds) && count(file($feeds) > 0)){
    #newlines
    $urls = file($feeds);
}else if (file_exists(dirname(__FILE__)."/assets/themes/$theme/$theme.xml")){
    #xml
    $themeX = simplexml_load_file(dirname(__FILE__)."/assets/themes/$theme/$theme.xml");
    if (!$themeX){
        debug("invalid $theme.XML");
    }else{
        foreach($themeX->feeds->url as $url){
            array_push($urls, $url);
        }
    }
}else{
    #default
    $urls = array(
        "http://reddit.com/r/jacksonville.rss",
    );
}

$itemcount = 0;
$posts = array();

foreach($urls as $url){
    debug("new feed: $url");
    if (substr($url,0,1) == "#"){
        debug("skipping $feed");
        continue;
    }

    $pie = new SimplePie();
    $pie->set_feed_url($url);
    $pie->enable_cache(true);
    $pie->force_feed(true);
    $pie->set_cache_location($cachepath);
    $pie->set_cache_duration(60 * 60 * 3); #3 hours
    $pie->set_timeout(30);
    $pie->init();
    if ($pie->error()){
        debug('pie error: '.$pie->error());
    }

    $feedcount = 0;             
    debug('item count: '.$pie->get_item_quantity());             
    foreach($pie->get_items(0, $feedmax) as $item){
        $post = array();
        $link = $item->get_link();

        $post['date'] = date("D, F d Y g:i:s a", $item->get_date('U'));        
        if ($post['date'] == null || $post['date'] == ''){  
            $pubDate = $item->get_item_tags('', 'pubDate');
            if ($pubDate != null){
                $post['date'] = date("D, F d Y g:i:s a", strtotime($pubDate[0]['data']));
            }
        }
        
        #if item is too old
        if (strtotime($post['date']) < strtotime($saveitemdate, date('U'))){
            debug('skipping old date: '.$post['date']);
            continue;
        }
        $date = $post['date'];
                
        if (false !== isCached($link)){
            $post = isCached($link);            
            debug("found cached item ($itemcount): ".$post['link']);
            if (validateCacheItem($post)){
                $itemcount++;
                array_push($posts, $post);
                continue;
            }
        }
   
        debug("new uncached item: $link");
        
        $post['date'] = $date;
        if ($post['date'] == null || $post['date'] == "") debug("!!! uncached date empty: ".$post['date']);        
        
        $post['link'] = $link;    
        $post['title'] = $item->get_title();
    
        $feed = $item->get_feed();
        $post['source'] = $feed->get_title();
        $post['sourceURL'] = $feed->get_link();
        unset($feed);
     
        $post['text'] = $item->get_content();
        
        $enclosure = $item->get_enclosure();
        if ($enclosure != null){
            $thumbs = $enclosure->get_thumbnails();
            if (count($thumbs) > 0){
                foreach($thumbs as $thumb){
                    if (false !== ($id = getYoutubeIdFromText($thumb))){
                        $post['youtube'] = $id;
                        debug("--thumb youtube id: $id");
                        $post['image'] = getYoutubeThumb($id);
                        debug("--youtube shot: ".$post['image']);                
                    }
                }
            }else{
                if (false !== ($id = getYoutubeIdFromText($thumbs))){
                    $post['youtube'] = $id;
                    debug("--thumb youtube id: $id");
                    $post['image'] = getYoutubeThumb($id);
                    debug("--youtube shot: ".$post['image']);                
                }
            }
        }

        //work with reddit
        if (stripos($link, "reddit.com") !== false ){
            $post = handleReddit($post);
        }

        if (isset($post['image']) && $post['image'] != ''){
            debug('--skipping image scrape: '.$post['image']);
        }else if (preg_match('/https?:\/\/.*(jpg|jpeg|gif|png)$/', $post['link'])){
            $post['image'] = $post['link'];
            debug("--link goes to image: ".$post['image']);
        }else if (null != ($img = ifImgur($post['link']))){
            $post['image'] = $img;
            debug("--link goes to imgur: ".$post['image']);
        }else if (null != ($id = ifYoutube($post['link']))){     
            $post['youtube'] = $id;
            $post['image'] = getYoutubeThumb($id);
            debug("--link goes to youtube: ".$post['image']);
        }else if (null != ($id = ifVimeo($post['link']))){
            $post['vimeo'] = $id;
            $post['image'] = getVimeoThumb($id);
            debug("--link goes to vimeo: ".$post['image']);
        }else{
            #by description
            if (null != ($id = ifYoutube($post['text']))){
                debug("--found youtube in description: $id");        
                $post['youtube'] = $id;
                $post['image'] = getYoutubeThumb($id);
            }else if (null != ($id = ifVimeo($post['text']))){
                debug("--found vimeo in description: $id");
                $post['image'] = getVimeoThumb($id); 
                $post['vimeo'] = $id;
            }else if (ifImgur($post['text'])){
                $post['image'] = ifImgur($post['text']);     
            }else if (preg_match('/img.*?src\=(\"|\')([^\"\'\>]+)/i', $post['text']) > 0){
                #<img>
                preg_match_all('/img.*?src\=(\"|\')([^\"\'\>]+)/i', $post['text'], $matches);

                debug('--found '.count($matches[2]).' images in text');
                if (count($matches[2]) > 0){
                    $post['image'] = getBestImg($matches[2]);
                }else{
                    $post['image'] = $matches[2][0];          
                }
            }
            
            if (!isset($post['image']) || $post['image'] == null){
                #else nothing in feed, scrape page 
                $post['image'] = getImageFromPage($post['link']);
            }
            
            if (!isset($post['image']) || $post['image'] == null){
                #nothing on page, need to go deeper
                debug('going deeper');
                preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $post['text'], $matches);
                $fail = 0;
                while ($post['image'] == null && count($matches[0]) > 0){
                    debug('link: '.$matches[0][0].', #matches: '.count($matches[0]));
                    if (stripos($matches[0][0], "reddit.com/user") === false && $matches[0][0] != $post['link']){                    
                        $post['image'] = getImageFromPage($matches[0][0]);
                    }
                    array_splice($matches[0], 0, 1); 
                    $fail++;
                    if ($fail > 5) break;
                }
            }
        }// end image search

        #nuking this fucking image
        if (isset($post['image']) && stripos($post['image'], "feeds.feedburner.com/~r/") !== false) $post['image'] = null;
        if (isset($post['image']) && stripos($post['image'], "stats.wordpress.com/b.gif?host=") !== false) $post['image'] = null;
        if (isset($post['image']) && stripos($post['image'], "wplogo.png") !== false) $post['image'] = null;             
    
        if (array_key_exists('image',$post) && $post['image'] != null){

            #if selected image is inside text, don't display image twice, remove image from text
            if (stripos($post['text'], $post['image']) !== false){
                $imgurl = preg_quote($post['image'], '/');
                $post['text'] = preg_replace('/<img.*?src\=\s?(\"|\')'.$imgurl.'(\"|\')+[^>]*>/i', '', $post['text']);
            }
    
            $basename = basename($post['image']);
            $basename = preg_replace("/\?.*$/", "", $basename);
            $tile_image = dirname(__FILE__)."/assets/tile_images/".md5($post['image']).'.'.$basename;
            $tile_image_rel = "assets/tile_images/".md5($post['image']).'.'.$basename;
            debug("--image: ".$post['image']);
            debug("--tile_image: $tile_image");
            debug("--tile_image_rel: $tile_image_rel");
            if (!file_exists($tile_image) && $post['image'] != null){
                echo("-- processing new image\n");
                process_image($post['image'], 260, 260, $tile_image, "scale,trim");
            }
            #this shud be fixed somehwo
            if (file_exists($tile_image)){
                if (stripos($tile_image, '%') !== false) $tile_image = str_replace('%', '%25', $tile_image);
                $post['tile_image'] = $tile_image_rel;
                $post['tile_image_path'] = $tile_image;
            }
        }#else no image, will be assigned random
         
        array_push($posts, $post);
        $itemcount++;
        debug("done with post ($itemcount)");        
    }
}    

$posts = theMotherFuckinAlgorithm($posts);
debug('#posts: '.count($posts));

#echo json_encode($posts);
debug("--------end cache run---------");

$fp = fopen($serialItemsFile, 'w');
fwrite($fp, serialize($posts));
fclose($fp);

/*--------------------------------------------------*/
/*                                                  */
function theMotherFuckinAlgorithm($posts){
    #sort by date
    if (!usort($posts, "sortPostsByDate")){                                                                                            
        debug('fail sort');
    }
    
    #add random images
    foreach($posts as &$post){
        if (!isset($post['tile_image']) || $post['tile_image'] == '' || stripos($post['tile_image'], 'assets/img/random') !== false){
            $post['tile_image'] = randomTileImage();
        }
    }
    return $posts;
}

function sortPostsByDate($a, $b){
    $dateA = strtotime($a['date']);
    $dateB = strtotime($b['date']);

    if ($dateA == $dateB) return 0;
    return ($dateA > $dateB) ? -1 : 1;  
}

function randomTileImage(){
    global $random_image_array; 
    global $random_image_index;
    $dirname = dirname(__FILE__)."/assets/img/random";
    debug("getting random img\n");
    if (! $random_image_array){
        debug("array uninitialized, creating\n");
        $random_image_array = glob("$dirname/*");
    }
    $size = count($random_image_array);
    if (!$random_image_index){
        $random_image_index = floor(rand(0, $size));
    }
    if ($random_image_index >= $size){
        $random_image_index = 0;
    }
    debug("index $random_image_index\n");
    $file = $random_image_array[$random_image_index];
    debug("file = $file\n");
    $random_image_index++;
    $rel_path = "assets/img/random/".basename($file);
    return $rel_path;
}

function ifYoutube($text){
  #return id if valid, else null
  $id = null;
  $pattern = '#(youtu\.be|youtube\.com)(/embed/|/v/|/vi/|.*v=)([\w-]{10,12})#';
  $result = preg_match($pattern, $text, $matches);
  if ($result) { 
      $id = $matches[3];
  }
  return $id;
}
function ifVimeo($text){

  #return id if valid, else null
  $id = null;
  if (preg_match('#http:\/\/(player\.)?vimeo\.com#', $text) > 0){
    preg_match_all('#player\.vimeo\.com\/video/([^/^?^"^\']+)#', $text, $matches);
    $id = $matches[1][0];
  }
  return $id; 
}
function ifImgur($text){
    $img = null;
    if (preg_match('/imgur.com\/a\/([^"|^<]+)/', $text, $matches)){
        $id = $matches[1];
        $img =  getImgurAlbumImage($id);
    }else if (preg_match('/imgur.com\/gallery\/([^"|^\'|^<]+)/', $text, $matches)){
        $id = $matches[1];
        $img = "http://i.imgur.com/$id";
        $img = fixImgur($img);
    }else if (preg_match('/imgur.com\/([^"|^\'|^<]+)/', $text, $matches)){
        $id = $matches[1];
        $img = "http://i.imgur.com/$id";
        $img = fixImgur($img);
    }        
    return $img;
}
function getVimeoThumb($id){
    $info = file_get_contents("http://vimeo.com/api/v2/video/$id.json");
    if ($info == null) return null;
    $info = json_decode($info);
    $thumb = $info[0]->thumbnail_large;
    return $thumb;
}
function getYoutubeIdFromText($text) {
    #debug("getting youtube id: $url");
    $pattern = '#(youtu\.be|youtube\.com)(/embed/|/v/|/vi/|.*v=)([\w-]{10,12})#';
    $result = preg_match($pattern, $text, $matches);
    if ($result) {
        return $matches[3];
    }
    return false;
}
function getYoutubeThumb($id){
    return "http://img.youtube.com/vi/".$id."/0.jpg";
}
function getPostFromItem($item){
    #post needs to be object --> new post($item)
}

function handleReddit($post){
    global $server_path;
    debug("-- post is reddit: ".$post['link']);
    if (stripos($post['source'], "reddit.com") === false){
      $post['source'] = "reddit.com/r/".$post['source'];
    }

    //reddit stores the actual link inside the description, that should be main link
    preg_match('/<a href="([^"]+)">\[link\]<\/a>/', $post['text'], $matches);
    $post['link'] = $matches[1];

    //reddit ID might come in handy
    preg_match('/\/comments\/([^\/]+)/', $post['text'], $matches);
    $post['redditID'] = $matches[1];
    $post['redditLink'] = $post['link'];

    $post['text'] = str_replace("<br>", "", $post['text']);

    //comments
    $comments = file_get_contents("$server_path/assets/getRedditInfo.php?id=".$post['redditID']);
    $post['comments'] = $comments;

    return $post;
}
function getImgurAlbumImage($albumID){
    if ($albumID != null && $albumID != ""){
        $album = json_decode(file_get_contents("http://api.imgur.com/2/album/".$albumID.".json"));
        $image = $album->album->images[0]->links->original;
        return $image;
    }
    return null;
}
/*
function getImageFromRedditText($text){
    if (stripos($text, 'imgur.com/a') !== false){
        preg_match('/imgur.com\/a\/([^"|^<]+)/', $text, $matches);
        return getImgurAlbumImage($matches[1]);
    }else if (stripos($text, 'imgur.com') !== false){
        preg_match('/imgur.com\/([^"|^<]+)/', $text, $matches);
        return fixImgur("http://i.imgur.com/".$matches[1][0]);        	
    }
    return null;
}
*/
function fixImgur($link){
    if ((strpos($link,".jpeg") === false) && (strpos($link,".jpg") === false) && 
        (strpos($link,".png") === false) && (strpos($link,".apng") === false) && (strpos($link, ".gif") === false)){
            $link = $link.".jpg";
        }
    return $link;
}
function getImageFromPage($link){
    debug("-scraping page : $link");
    
    $text = @file_get_contents($link);
    if (!$text){
        debug("! cannot retrieve link $link");
        return null;
    }
    
    if (null != ($id = ifVimeo($text))){
        debug("--found vimeo id: $id \n");
        return getVimeoThumb($id); 
    }
    if (null != ($id = ifYoutube($text))){
        debug("--found youtube id: $id \n");
        return getYoutubeThumb($id);    
    }  
    
    $imgs = htmlqp($link, 'img');
    
    #get array of urls to pass to picking function
    $urls = array();
    foreach($imgs as $img){
        $src = $img->attr('src');
    
        if (stripos($src, "http") === false){
            #relative img paths
            if (stripos($src, "//") !== false){
                $src = "http:".$src;                    
            }else if (stripos($src, "/") === 0){
                #/image/...so path starts from root, get domain
                $domain = parse_url($link, PHP_URL_SCHEME)."://".parse_url($link, PHP_URL_HOST);
                $src = $domain.$src;        
            }else{
                #image/.. so path starts from cur directory, get cur directory
                preg_match_all('#(.*?/[^/])+$#', $link, $matches);
                $src = $matches[1][0].$src;
            }
        }
    
        array_push($urls, $src);
    }

    return getBestImg($urls);
}
/* accepts array of image URLS */
function getBestImg($imgs){
    $curImg = null;
    $curImgArea = 0;
    $count = 0;

    foreach($imgs as $img){
        if ($img == null || $img == "") continue;
                
        if (stripos($img, "feeds.feedburner.com/~r/") !== false ||
            stripos($img, "thumbs.redditmedia") !== false ||
            stripos($img, "wplogo.png") !== false){
            debug('--skip due to presets (reddit thumb, wplogo, etc.)');
            continue;
        }
        $size = getImageSize($img);    
        $area = $size[0] * $size[1];
        $dims = array($size[0], $size[1]);

        #ignore small images
        if ($area < 3000){
            #debug("---skip due to small area: $area");
            continue;
        }
        #ignore excessively long/wide images
        if ((max($dims) / min($dims)) > 1.8){
            #debug("---skip due to ratio");
            continue;
        }
    
        if ($area > $curImgArea){
            $count++;
            debug("---selecting image: $img - $area vs $curImgArea");
            $curImg = $img;
            $curImgArea = $area;
            #improves the hell out of links with lots of big images
            if ($count > 5 && $curImg != null || $curImgArea > 100000){
                debug("--count: $count, area: $curImgArea - returning");
                return $curImg;
            }            
        }

    }
    return $curImg;
}
function debug($msg){
  global $_DEBUG;
  if ($_DEBUG){ 
      error_log($msg);
      echo $msg;
  }
}
function isCached($link){
    global $cache;
    foreach($cache as $i=>$post){
        if (in_array($link, $post)){
            return $post;
        }      
    }
    return false;
}
function validateCacheItem(&$post){
    if ($post['date'] == null || $post['date'] == ""){
        debug("validate cached date empty: ".$post['date']);
        return false;
    }
    if (stripos($post['image_tile'], dirname(__FILE__).'/assets/img/random') !== false){
        #cached a random image, but let's try again
        debug("validate cached item is random image, rescanning");
        return false;
    }
    if (!file_exists($post['tile_image_path']) && $post['image'] != ''){
        echo('validate cached item has image but no tiled image, processing: '.$post['tile_image_path']."\n");
        process_image($post['image'], 260, 260, $post['tile_image_path'], "scale,trim");
        return true;
    }
    if (!isset($post['image']) || !isset($post['tile_image'])){
        #no image, try to find one
        debug("validate cached item has no image || no tile_image");
        return false;
    }    

    #if i don't want to read cache for an item for any reason
    #if (stripos($post['link'], 'jaxbikecoalition') !== false) return false;

    return true;    
     
}
?>
